import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import MainPage from './containers/MainPage/MainPage';
import MessagesHomework from './containers/MessagesHomework/MessagesHomework';
import MoviesList from './containers/MoviesList/MoviesList';

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={MainPage} />
        <Route path="/chatroom" component={MessagesHomework} />
        <Route path="/movies" component={MoviesList} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
