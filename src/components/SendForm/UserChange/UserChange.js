import React from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Container from 'react-bootstrap/Container';

const UserChange = props => {

    

    return (
        <>
            <div>
                <Button
                    variant="primary"
                    onClick={props.handleShow}
                >
                    Change User
                </Button>
                <Button
                    variant="secondary"
                    style={{ marginLeft: "10px" }}
                    onClick={props.returnToMainPage}
                    >
                    Return to Main Page
                </Button>
            </div>
            <Modal show={props.show} onHide={props.handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Change User</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Container>
                        <InputGroup>
                            <FormControl
                                placeholder="Enter new username"
                                id="NewUser"
                            />
                            <InputGroup.Append>
                                <Button variant="primary" onClick={props.changeUser}>Save</Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Container>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={props.handleClose}>
                        Close
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
};

export default UserChange;