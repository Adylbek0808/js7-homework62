import Button from 'react-bootstrap/Button';
import React from 'react';
import './SendForm.css'
import Form from 'react-bootstrap/Form';
import UserChange from './UserChange/UserChange';

const SendForm = props => {
    return (
        <div >
            <Form onSubmit={props.submit}>
                <Form.Group>
                    <div className="UserInfo">
                        <p className="UserName"> Current user: <b>{props.user}</b></p>
                        <UserChange
                            show={props.show}
                            handleClose={props.handleClose}
                            handleShow={props.handleShow}
                            changeUser={props.changeUser}
                            user={props.user}
                            returnToMainPage={props.returnToMainPage}
                        />
                    </div>
                    <Form.Label>Enter your message:</Form.Label>
                    <Form.Control
                        as="textarea"
                        rows={3}
                        id="MessageField"
                    />
                </Form.Group>
                <Button variant="primary" type="submit">
                    SEND
                 </Button>
                 
            </Form>
        </div>
    );
};

export default SendForm;