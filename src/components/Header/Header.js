import React from 'react';
import './Header.css';

const Header = props => {
    return (
        <header className="header">
            <p className="head_logo">{props.logoName}</p>
            <input type="text" id="search" placeholder="Enter search item" />
            <button type="button">Search</button>
        </header>
    );
};

export default Header;