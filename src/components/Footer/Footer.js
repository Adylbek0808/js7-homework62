import React from 'react';
import './Footer.css';

const Footer = props => {
    return (
        <footer className="footer">
            <div className="container footer_int">
                <p >{props.copyrightName}</p>
                <p>{props.email} <span>tel: {props.tel}</span> </p>
            </div>
        </footer>
    );
};

export default Footer;