import React from 'react';
import './Sidebar.css'

const Sidebar = props => {
    return (
        <div className="sidebar">
            <nav>
                <ul>
                    <h4>Choose activity:</h4>
                    <li><p onClick={props.showMovies}>Show our favourite movies</p></li>
                    <li><p onClick={props.enterChat}>Chat with our clients</p></li>
                </ul>
            </nav>
        </div>
    );
};

export default Sidebar;