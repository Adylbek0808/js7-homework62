import React from 'react';
import './NameCard.css';

const NameCard = props => {
    return (
        <div className="name_card">
            <img src={props.img} alt="#" />
            <p className="name">{props.name}</p>
            <p>Position: {props.position}</p>
        </div>
    );
};

export default NameCard;