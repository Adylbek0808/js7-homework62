import React from 'react';
import NameCard from './NameCard/NameCard';
import './NamesDisplay.css';

const NamesDisplay = props => {


    const list = props.people.map(person => (
        <NameCard
            key={person.id}
            img={person.img}
            name={person.name}
            position={person.position}
        />
    ));
    return (
        <div className="main_display">
            <h2>Our People:</h2>
            <div className="card_board">
                {list}
            </div>

        </div>
    );
};

export default NamesDisplay;