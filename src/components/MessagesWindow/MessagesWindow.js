import React from 'react';
import Message from './Message/Message';
import './MessagesWindow.css';

const MessagesWindow = props => {
    const posts = props.posts.map(post => (
        <Message key={post._id} author={post.author} message={post.message} time={post.datetime} />
    ));
    return (
        <>
            <h3>Recent posts:</h3>
            <div className="recentPosts">
                {posts}
            </div>
        </>
    );
};

export default MessagesWindow;