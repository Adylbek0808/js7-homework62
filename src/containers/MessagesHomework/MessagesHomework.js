import React, { useEffect, useState } from 'react';
import MessagesWindow from '../../components/MessagesWindow/MessagesWindow';
import SendForm from '../../components/SendForm/SendForm';
import './MessagesHomework.css';

const url = "http://146.185.154.90:8000/messages";

const MessagesHomework = props => {

    const [message, setMessage] = useState({
        user: "Adyl",
        message: ""
    });
    const [posts, setPosts] = useState([]);
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const changeUserName = () => {
        const userCopy = message;
        userCopy.user = document.getElementById("NewUser").value;
        setMessage(userCopy);
        handleClose();
    };

    const postMessage = async () => {
        const data = new URLSearchParams();
        data.set('message', message.message);
        data.set('author', message.user);
        const response = await fetch(url, {
            method: 'post',
            body: data
        });
        console.log(response);
    };
    const submitMessage = e => {
        e.preventDefault();
        console.log("Old message: ", message);
        const messageCopy = message;
        messageCopy.message = document.getElementById("MessageField").value;
        setMessage(messageCopy);
        postMessage();
        document.getElementById("MessageField").value = '';
        console.log("New message: ", message);
    };

    const returnToMainPage = () => {
        props.history.goBack();
    };

    useEffect(() => {
        let lastMessageDate;
        const fetchData = async () => {
            const response = await fetch(url);
            if (response.ok) {
                const postsNew = await response.json();
                setPosts(postsNew.reverse());
                console.log(postsNew);
                lastMessageDate = postsNew[0].datetime;
            };
        };
        const checkforNewMessages = async () => {
            const response = await fetch(url + "?datetime=" + lastMessageDate);
            if (response.ok) {
                const posts = await response.json();
                if (posts.length > 0) {
                    fetchData().catch(e => console.error(e));
                    clearInterval();
                }
                console.log("New posts:", posts.length, posts);
            };
        };
        fetchData().catch(e => console.error(e));
        const interval = setInterval(() => {
            checkforNewMessages().catch(e => console.error(e))
        }, 1000);
        return () => clearInterval(interval);
    }, []);

    return (
        <div className="Container">
            <SendForm
                submit={submitMessage}
                user={message.user}
                show={show}
                handleClose={handleClose}
                handleShow={handleShow}
                changeUser={changeUserName}
                returnToMainPage={returnToMainPage}
            />
            <MessagesWindow posts={posts} />
        </div>
    );
};

export default MessagesHomework;