import React, { useRef } from 'react';

import Footer from '../../components/Footer/Footer';
import Header from '../../components/Header/Header';
import Sidebar from '../../components/Sidebar/Sidebar';
import NamesDisplay from '../../components/NamesDisplay/NamesDisplay';


import Funt from '../../images/People/funt.jpg';
import Bender from '../../images/People/bender.jpg';
import Panikovskiy from '../../images/People/panikovskiy.jpg';
import Balaganov from '../../images/People/balaganov.jpg';
import Kozlevich from '../../images/People/kozlevich.jpg';


import './MainPage.css';


const PEOPLE = [
    { id: 1, img: Funt, name: "Sitzchairman Funt", position: "CEO" },
    { id: 2, img: Bender, name: "Ostap Bender", position: "PR Manager" },
    { id: 3, img: Panikovskiy, name: "Mikhail Panikovsiy", position: "Procurement Officer" },
    { id: 4, img: Balaganov, name: "Shura Balaganov", position: "Security Officer" },
    { id: 5, img: Kozlevich, name: "Adam Kozlevich", position: "Driver" }
]



const MainPage = props => {

    const people = useRef(PEOPLE);

    const enterChatRoom = () => {
        props.history.push({
            pathname: '/chatroom'
        });
    };

    const showMoviesHandler = () => {
        props.history.push({
            pathname: '/movies'
        });
    };


    return (
        <>
            <Header logoName='Hornes and Hooves inc.' />
            <div className="main">
                <Sidebar
                    enterChat={enterChatRoom}
                    showMovies={showMoviesHandler}
                />
                <NamesDisplay people={people.current} />
            </div>
            <Footer
                copyrightName="© Hornes and Hooves inc."
                email="O.Bender@HH.com"
                tel="0555 123 456 789"
            />
        </>
    );
};

export default MainPage;