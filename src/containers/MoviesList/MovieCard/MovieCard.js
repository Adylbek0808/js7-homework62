import React from 'react';
import './MovieCard.css'

const MovieCard = props => {
    return (
        <div className="card">
            <div className="imgBox">
                <img src={props.img} alt="#" className="cardImg" />
            </div>
            <div className="infoBox">
                <h1 className="name">{props.name}</h1>
                <p>Жанр: {props.genre}</p>
                <p>Год выхода: {props.year}</p>
            </div>
        </div>
    );
};

export default MovieCard;