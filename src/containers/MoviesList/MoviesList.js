import React from 'react';


import YesMinister from '../../images/Movies/Yes_Minister.jpg';
import ScannerDarkly from '../../images/Movies/Scanner_Darkly.jpg';
import DrStrangelove from '../../images/Movies/Dr_Strangelove.jpg';
import MovieCard from './MovieCard/MovieCard';

import './MoviesList.css';
import Button from 'react-bootstrap/Button';

const MOVIES = [
    { id: 1, name: "Да, господин министр", genre: "Сатирический сериал", year: "1980-1984", img: YesMinister },
    { id: 2, name: "Помутнение", genre: "Фантастика", year: "2006", img: ScannerDarkly },
    { id: 3, name: "Доктор Стрейнджлав", genre: "Комедия", year: "1964", img: DrStrangelove }
]


const MoviesList = props => {
    const moviesList = MOVIES.map(movie => (
        <MovieCard
            key={movie.id}
            name={movie.name}
            genre={movie.genre}
            year={movie.year}
            img={movie.img}
        />
    ));

    const returnToMainPage = () => {
        props.history.goBack();
    };
    return (
        <div>
            <div className="MoviesList">
                {moviesList}
            </div>
            <Button
                variant="primary"
                onClick={returnToMainPage}
            >
                Return to Main Page
            </Button>
        </div>
    );
};

export default MoviesList;